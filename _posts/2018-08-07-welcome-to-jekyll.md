---
layout: post
title: Introducing Jasper as gem
date: '2018-08-07 18:29:59 +0000'
category: New
author: Yashu Mittal
---
Creating a Jasper theme was a great step.

**Big news today**, we're extracting the files from [Jasper theme](https://www.gitlab.com/mittalyashu/jasper) and creating a gem out of it.

No more cloning the repository, just add the following code:

In the `_config.yml` file

{% highlight yaml %}
theme: jasper-gem
{% endhighlight %}

In the `Gemfile` file

{% highlight Gemfile %}
gem "jasper-gem", "~> (latest version)"
{% endhighlight %}

You are good to 🚀 launch your brand Jekyll site using the Jasper theme.

Ahhh! Don't forget to run this command `bundle install` to install the jasper theme.
