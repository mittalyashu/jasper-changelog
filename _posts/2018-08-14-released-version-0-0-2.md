---
layout: post
title: Released version 0.0.2
date: 2018-08-12T04:48:04.650Z
category: Update
author: Yashu Mittal
---
After 24 hours of launching [Jasper 👻 gem publicity](gem-is-now-public/), we got more than 50 downloads and we thriller to see this progress.

Keep track of your users on the website by using Google Analytics 📊, just add the tracking id in `_config.yml` file.

Add comment 💬 section by enabling Disqus and adding `disqus_shortname` in the `_config.yml` file.

If you are a developer 👨‍💻, now you showoff your GitHub/GitLab profile.
