---
layout: post
title: "\U0001F48E is now public"
date: 2018-08-08 06:30:00 +0000
category: Announcement
author: Yashu Mittal

---
Jasper gem is now publically available to download from [RubyGem](https://rubygems.org/gems/jasper-theme).

Found any bugs 🐛? We welcome 🙏 your [issues and pull requests](https://gitlab.com/mittalyashu/jasper-theme).